#ifndef APPLICATIONS_H
#define APPLICATIONS_H

#define DESKTOP_FILES_FOLDER_USER QDir::homePath() + "/.local/share/applications"
#define DESKTOP_FILES_FOLDER_SYSTEM "/usr/share/applications"

#include <QObject>
#include <QDir>
#include <QQmlListProperty>
#include <QProcess>
#include <QSettings>

#include "app.h"

class Applications: public QObject {
    Q_OBJECT

    Q_PROPERTY(QQmlListProperty<App> list READ list NOTIFY listChanged)
    Q_PROPERTY(QQmlListProperty<App> selectedList READ selectedList NOTIFY selectedListChanged)
    Q_PROPERTY(bool isDashSet READ isDashSet NOTIFY isDashSetChanged)

public:
    Applications();
    ~Applications() = default;

    Q_INVOKABLE void refresh();
    Q_INVOKABLE App* get(const QString &appId);
    Q_INVOKABLE void selectApp(const QString &id);
    Q_INVOKABLE void removeApp(const QString &id);
    Q_INVOKABLE bool setDash();
    Q_INVOKABLE bool unsetDash();

    QQmlListProperty<App> list();
    QQmlListProperty<App> selectedList();
    bool isDashSet() const;
    bool getIsDashSet();

Q_SIGNALS:
    void listChanged();
    void selectedListChanged();
    void isDashSetChanged();

private:
    Q_DISABLE_COPY(Applications)

    void loadDesktopFiles(const QString &path);
    void loadSelected();

    QList<App *> m_applications;
    QList<App *> m_selectedApplications;
    QSettings m_settings;
    bool m_appsLoaded = false;
    int m_numLauncher = 0;
    bool m_isDashSet = false;
    QString m_dashOverride;
};

#endif
